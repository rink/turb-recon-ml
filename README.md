# Turbulence reconstruction using machine learning 

Code for Peter and Christoffer's Bachelor project, spring 2020.

## Adding files to repo

In an Anaconda prompt (or another terminal that includes `git`).

1. `cd` to the directory where you want the local folder to be located.  
   ```
   cd <top_folder>
   ```
2. Clone the empty folder from the server.  
   ```
   git clone https://gitlab.windenergy.dtu.dk/rink/turb-recon-ml.git
   ```
3. Copy the files that you want stored on the server to the folder.
   Binary files (e.g., pngs, PDFs, compiled code, etc.) should be avoided due to
   the nature of git repositories, but we'll let it slide for this repo.
4. Add, commit and push the files.  
   ```
   git add -A
   git commit -am "adding files"
   git push origin master
   ```
5. Check the files are on the website.  
   [Go to website](https://gitlab.windenergy.dtu.dk/rink/turb-recon-ml)
6. You can delete the local copy of the folder if you want.